package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.dto.Tag;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class TagRepository implements PanacheRepository<Tag> {

    public Tag findByLabel(String label) {
        return find("label", label).firstResult();
    }
}
