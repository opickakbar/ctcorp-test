package org.acme.service;

import org.acme.dto.Tag;

import javax.ws.rs.core.Response;

public interface TagService {
    Response insertTag (Tag tag);
    Response updateTag (Long id, Tag tagRequest);
    Response deleteTag (Long id);
    Response getSingleTag (Long id);
    Response getAllTags ();
}
