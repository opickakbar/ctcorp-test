package org.acme.service;

import org.acme.dto.Post;

import javax.ws.rs.core.Response;

public interface PostService {
    Response insertPost (Post post);
    Response updatePost (Long id, Post postRequest);
    Response deletePost (Long id);
    Response getSinglePost (Long id);
    Response getAllPosts ();
}
