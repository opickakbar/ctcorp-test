package org.acme.service.impl;

import org.acme.dto.Tag;
import org.acme.repository.TagRepository;
import org.acme.service.TagService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class TagServiceImpl implements TagService {

    @Inject
    private TagRepository tagRepository;

    @Override
    public Response insertTag(Tag tag) {
        tagRepository.persist(tag);
        if (tagRepository.isPersistent(tag)) {
            return Response.ok("Insert success!").build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity("Insert failed").build();
    }

    @Override
    public Response updateTag(Long id, Tag tagRequest) {
        Tag tag = Optional.ofNullable(tagRepository.findById(id))
                .orElseThrow(NotFoundException::new);
        tag.setLabel(tagRequest.getLabel());
        tagRepository.persist(tag);
        return Response.ok("Update success!").build();
    }

    @Override
    public Response deleteTag(Long id) {
        boolean deleted = tagRepository.deleteById(id);
        if (deleted) {
            return Response.ok("Delete success").build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity("Delete failed").build();
    }

    @Override
    public Response getSingleTag(Long id) {
        return tagRepository.findByIdOptional(id)
                .map(tag -> Response.ok(tag).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).entity("Data not found").build());
    }

    @Override
    public Response getAllTags() {
        List<Tag> tags = tagRepository.listAll();
        return Response.ok(tags).build();
    }
}
