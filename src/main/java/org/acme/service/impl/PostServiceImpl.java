package org.acme.service.impl;

import org.acme.dto.Post;
import org.acme.dto.Tag;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;
import org.acme.service.PostService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.*;

@ApplicationScoped
public class PostServiceImpl implements PostService {

    @Inject
    private TagRepository tagRepository;

    @Inject
    private PostRepository postRepository;

    @Override
    public Response insertPost(Post postRequest) {
        Set<Tag> tagList = getAllTags(postRequest);
        Post newPostData = new Post();
        newPostData.setTitle(postRequest.getTitle());
        newPostData.setContent(postRequest.getContent());
        newPostData.setTags(tagList);
        postRepository.persist(newPostData);
        if (postRepository.isPersistent(newPostData)) {
            return Response.ok("Insert success!").build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity("Insert failed").build();
    }

    @Override
    public Response updatePost(Long id, Post postRequest) {
        Post post = Optional.ofNullable(postRepository.findById(id))
                .orElseThrow(NotFoundException::new);
        post.setTitle(postRequest.getTitle());
        post.setContent(postRequest.getContent());
        if (Objects.nonNull(postRequest.getTags())) {
            Set<Tag> tagList = getAllTags(postRequest);
            post.setTags(tagList);
        }
        postRepository.persist(post);
        return Response.ok("Update success!").build();
    }

    @Override
    public Response deletePost(Long id) {
        boolean deleted = postRepository.deleteById(id);
        if (deleted) {
            return Response.ok("Delete success").build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity("Delete failed").build();
    }

    @Override
    public Response getSinglePost(Long id) {
        return postRepository.findByIdOptional(id)
                .map(post -> Response.ok(post).build())
                .orElse(Response.status(Response.Status.NOT_FOUND)
                        .entity("Data not found")
                        .build());
    }

    @Override
    public Response getAllPosts() {
        List<Post> posts = postRepository.listAll();
        return Response.ok(posts).build();
    }

    private Set<Tag> getAllTags(Post post) {
        Set<Tag> result = new HashSet<>();
        for (Tag tag : post.getTags()) {
            Tag tagData = tagRepository.findByLabel(tag.getLabel());
            if (Objects.isNull(tagData)) {
                tagRepository.persist(tag);
                result.add(tag);
            } else {
                Tag tempTag = new Tag();
                tempTag.setId(tagData.getId());
                result.add(tempTag);
            }
        }
        return result;
    }
}
